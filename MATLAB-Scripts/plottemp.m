 figure(1)
 plot(tempf1);
 set(gca,'XTick',1:1:length(tempc1))
 set(gca,'XTickLabel',time1);
 title('7F48-temp(F)');
 
 figure(2)
 plot(tempc1);
 set(gca,'XTick',1:1:length(tempc1))
 set(gca,'XTickLabel',time1);
 title('7F48-temp(C)');



figure(3)
plot(tempf2);
set(gca,'XTick',1:1:length(tempc2))
set(gca,'XTickLabel',time2);
title('7D2E-temp(F)');

figure(4)
plot(tempc2);
set(gca,'XTick',1:1:length(tempc2))
set(gca,'XTickLabel',time2);
title('7D2E-temp(C)');