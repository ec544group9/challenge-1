clc;
clear;
format long g; % make the print data looks well

while 1
    

pause(2)

%read the data from text file
[col1 col2 col3 col4] = textread('spotData.txt', '%d %s %f %f', 'delimiter', ' ');

number=unique(col2); %name of sensors
len=length(number);  %number of sensors

%for every sensor,create 3 arrays to store tempf,tempc and time
for i=1:length(number)
    h=1;
    for j=1:length(col2)
     
        if strcmp(col2(j), number(i))
         
         
         eval(['tempf' int2str(i) '(h)=col3(j);']);
         eval(['tempc' int2str(i) '(h)=col4(j);']);
         eval(['time' int2str(i) '(h)=col1(j);']);
        
          h=h+1;
        end
     
    end
end
 
    
    
    for e=1:len
        
         eval(['resultf(e)=tempf' int2str(e) '(end);']);
         eval(['resultc(e)=tempc' int2str(e) '(end);']);
         eval(['resulttime(e)=time' int2str(e) '(end);']);
    
    end
    
   result=[number';num2cell(resulttime);num2cell(resultf);num2cell(resultc)];
    
    %display the sensors' serial number and their temperature
     
    disp(result);


    
    
end