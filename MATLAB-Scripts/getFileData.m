%% Reads in the file at the location, and places it into a map of structures
function data = getFileData(fileLocation)
    fid = fopen(fileLocation,'r');
    output = textscan(fid, '%s %s %f %f', 'delimiter', ' ');
    time = output{1};
    name = output{2};
    fahr = output{3};
    cels = output{4};
    
    data = containers.Map;
    for ii = 1:length(time)
        key = name{ii};
        if (isKey(data, key))
           % Add this object in to the current key map 
           toAdd = struct('time', time{ii}, 'fahrenheit', fahr(ii), 'celsius', cels(ii));
           data(key) = [ data(key) toAdd ];
        else
           % The value is not in the dictionary yet, add it in
           data(key) = struct('time', time{ii}, 'fahrenheit', fahr(ii), 'celsius', cels(ii));
        end
    end
    
    % Data is returned with the appropriate rows 
    fclose(fid);
end