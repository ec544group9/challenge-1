%% Prints out the input data to the screen in a nice manner 
function printData(data)
    motes = keys(data);
    
    time = {};
    fahr = [];
    cels = [];
    for ii = 1:length(motes)
       moteData = data(motes{ii});
       mostRecent = moteData(length(moteData));
       time = [time mostRecent.time];
       fahr = [fahr mostRecent.fahrenheit];
       cels = [cels mostRecent.celsius];
    end
    
    % Create a display cell structure 
    moteRow = ['Motes' motes];
    timeRow = ['Time' time];
    fahrRow = ['Fahrenheit' num2cell(fahr)];
    celsRow = ['Celsius' num2cell(cels)];
    displayCells = [ moteRow ; timeRow ; fahrRow ; celsRow ]; 
    disp(displayCells);
end