%% Looks at output data file, parses the data into segments 

clear all;
close all;

% Do the original 
workingDir = pwd;
fileLocation = strcat(workingDir, '/spotData.txt');

fileInfo = dir(fileLocation);
originalSize = fileInfo.bytes;

data = getFileData(fileLocation);
printData(data);
fprintf('\n');

% Loop repeatedly for the data
while (1)
    fileInfo = dir(fileLocation);
    fileSize = fileInfo.bytes;
    if (fileSize == originalSize)
        % Nochange, just continue
        continue;
    end
    
    % a change occurred, begin analysis again to print out information
    originalSize = fileSize; % update size
    data = getFileData(fileLocation);
    printData(data);
    
    % Pause for 1 second
    pause(1);
end
    
    
