Challenge 1 - Group 9

The code for our challenge 1 is based off of the SendData demo provided in the SunSPOT SDK.

Challenge1-OnSpot folder is the netbeans project that contains the code that is sent to each
mote for taking temperature readings.  Challenge1-OnDesktop contains the basestation code
for retrieving all sensor data from the motes, and sending them to the desktop for saving to 
a file.

The MATLAB-Scripts folder contains the scripts we used for display.  In particular, we used the 
scripts:
  realTimeDataDisplay.m
  getFileData.m
  printData.m

Special Note:  The challenge 1 on desktop must save the spot text file to the MATLAB-Scripts 
folder.  To run on your computer, you must change line 71 in the SendDataDemoHostApplication.java
file from:

File file = new File("/Users/Raphy/Documents/School/Fall 2013/EC544/Challenge 1/MATLAB-Scripts/spotData.txt");

to

File file = new File("/Path/to/challenge/folder/MATLAB-Scripts/spotData.txt");

where the path to challenge folder is replaced with the path to the root directory of the project on your
computer.

